import mitmproxy
from mitmproxy import ctx
from mitmproxy import http
import re

# hook on mitm response
def response(flow):
    if flow.request.pretty_host.find('wikipedia.org') != -1 and flow.request.path.find('api.php?action=opensearch') != -1:
        res_str = flow.response.content.decode('utf-8')
        # replace php,python,scala with javascript
        php_pattern = re.compile("php", re.IGNORECASE)
        python_pattern = re.compile("python", re.IGNORECASE)
        scala_pattern = re.compile("scala", re.IGNORECASE)
        res_str = php_pattern.sub("Javascript", res_str)
        res_str = python_pattern.sub("Javascript", res_str)
        res_str = scala_pattern.sub("Javascript", res_str)
        # send the response
        flow.response.content = bytes(res_str,'utf-8')

