import mitmproxy
from mitmproxy import ctx
from mitmproxy import http

# hook on mitm repsponse
def request(flow):
    # override wikipedia's quick search api with a static response
    if flow.request.pretty_host.find('wikipedia.org') != -1 and flow.request.path.find('api.php?action=opensearch') != -1:
        apiResponse = '''["Javascript everywhere",["JavaScript","JavaScript syntax","Javascript test","Javascripot syntax","Javascript Pages","Javascript","Javascript forever","Javascript forever","Javascript forever","Javascript forever"],["","","","","","","","","",""],["https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript","https://en.wikipedia.org/wiki/JavaScript"]]'''
        flow.response = http.HTTPResponse.make(
            200,
            apiResponse,
            {"Content-Type": "application/json; charset=UTF-8"}
        )
    # override when reading wikipedia's javascript page
    if flow.request.pretty_host.find('wikipedia.org') != -1 and flow.request.path.find('/wiki/JavaScript') != -1:
        with open("/home/mitmproxy/javascript.html","r") as f:
            fileReponse = f.read()
        flow.response = http.HTTPResponse.make(
            200,
            fileReponse,
            {"Content-Type": "text/html; charset=UTF-8"}
        )
