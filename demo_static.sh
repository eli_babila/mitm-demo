#!/bin/bash
docker run --rm -it -p 0.0.0.0:8080:8080 -v $(pwd)/mitm:/home/mitmproxy -v ~/.mitmproxy:/home/mitmproxy/.mitmproxy mitmproxy/mitmproxy:dev mitmproxy --mode socks5 -s /home/mitmproxy/wikipedia_static_response.py
